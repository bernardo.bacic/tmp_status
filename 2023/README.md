# Week 20 Oct - 26 Oct 2023
 * More Grafana dashboards for Low CBF
   - Show processor's Delay Polynomials in a dashboard (requires processor code changes)
   - system demo slides for Grafana
   - PERENTIE-2110 Grafana-EDA setup
   - testing: Tango attribute polling vs push
   - investigating double attribute entries in TimescaleDB
   - show FPGA registers on dashboard (temperature, power, bad Ether/UDP packets...)
 * Code reviews
# Week 13 Oct - 19 Oct 2023
 * More Grafana dashboards for Low CBF
   - changing the underlying Low CBF code to expose Tango attributes an operator might be interested in
 * Code reviews
# Week 6 Oct - 12 Oct 2023
 * Grafana dashboards for Low CBF
   - modifications to Tango attribute flags
   - support for multiple Alveo cards
   - debugging misaligned event reporting 
   - local (`psi-perentie1`) installation of Grafana (recent version)
 * Code reviews
 
# Week 29 Sep - 5 Oct 2023
 * Getting familiar with Grafana [PERENTIE-2160](https://jira.skatelescope.org/browse/PERENTIE-2160)
   - deploying (TimescaleDB, Grafana) in `minikube`
   - deploying in Low PSI - in Docker containers
   - dashboard panels for CBF `healthState` attributes (processor, subarray, controller)
 * `systemd` service for P4 switch [PERENTIE-2078](https://jira.skatelescope.org/browse/PERENTIE-2078)
 * Code reviews
 * Admin
   - PI#20 planning meeting in Rome: plane ticktes, accommodation
   
# Week 22 Sep - 28 Sep 2023
 * systemd service and an Ansible script to configure SDE on Low PSI [P4 switch](https://jira.skatelescope.org/browse/PERENTIE-2078)
 * GitLab scheduled pipleine:
   - configure once-a-week [integration test](https://jira.skatelescope.org/browse/PERENTIE-2076) to excercise major CBF components together
   - Slack channel notificiation in case of pipeline failure
 * DEBUGGING
   - GitLab CI/CD job randomly fails due to (probably) a race condition in Makefiles; discussed with Andrew and he's raised a support ticket with the Systems Team
   - Ansible failing: doesn't like my LC_ALL and LC_LANG environment variables - requires UTF-8 encoding
   - investigate incompatibilities between the latest PROCESSOR software and CORRELATOR firmware
   - GitLab CI/CD job randomly fails as it cannot fetch Python packages from PyPI
   - GitLab CI/CD job can fail due to a dodgy deployment: some containers are way older than others, possibly spill over from a previous run that was not cleaned up properly
 * Finished [Integration test work](https://jira.skatelescope.org/browse/PERENTIE-2071)
 * BAU: code reviews, 
 
# Week 15 Sep - 21 Sep 2023
 * adding test cases to GitLab Integration Test
 * updates to `lsalveo` utility
 * debugging various issues
   - CBF no longer deploys in Low PSI; due to main branch changes
   - Integration test various issues: SW compatiblity problem, internal interface change, P4 switch port activation, CNIC-VD needs duplex mode enabled
 * code reviews
 * documentation updates [MR-5](https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-integration/-/merge_requests/5)
   [MR-10](https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-tango-cnic/-/merge_requests/10)
 * meetings: RELEASE process
   - [Ruby's thoughts](https://docs.google.com/document/d/1TwZRKjBMQAkoM89EOBIrZmyfAXtmBww220IabAP10oY/edit#heading=h.ffrx7xlxvtlx)
   - [Grant's thoughts](https://docs.google.com/presentation/d/1PDn6xC2rDFYiANjYgfUSvD9V0VDPcJ0pCALCL0vvl9Q/edit#slide=id.g2457e13e050_0_10)
 * ADMIN: chasing invoices, FlexiPurchase
 
# Week  1 Sep - 7 Sep 2023
 * PI#20 planning week in Perth
 * still looking at GitLab CI/CD `low-psi-test` test failures
 
# Week 28 Aug - 31 Aug 2023
 * GitLab CI/CD debugging `low-psi-test` test failures; multiple causes
   - using wrong version of correlator FW in which some of the FPGA registers are missing and causing exception in the SW part
   - a long standing issue with `low-psi-test` where the P4 switch required `LoadPorts()` command was missing; it was masked by the fact that on the live system users would manually activate port through such command either in `IPython` or a Jupyter notebook
   - P4 switch pods in the cluster were intermittently restarting thus affecting `CNIC` traffic; needed a redeployment
   - CORBA (Tango device proxy) calls on Subarray device timeout
   
# Week 21 Aug - 24 Aug 2023
 * Low CBF health state
     - update RTD documentation 
     - painful debugging of things gone wrong after merging main (git) branch
     - update System Demo slides
 * CSIRO compulsory Unconscious Bias course

# 25 July - 18 Aug 2023
 * annual leave

# Week 14 June - 20 July 2023
 * PROCESSOR and CONNECTOR healthState: 
     - document the design
     - restructure code as suggested in code review

 * Taranta dashboards
     - show general and Correlator specific information
	   [PERENTIE-1981](https://jira.skatelescope.org/browse/PERENTIE-1981)
	   
 * expose Alveo temperature/power/etc as Tango attribute
     - [PERENTIE-1978](https://jira.skatelescope.org/browse/PERENTIE-1978)
	 - problems with Poetry dependencies: had to remove CNIC from PROCESSOR 
	   module (as there's a new Tango version of CNIC-VD)
	   
# Week 7 June - 13 July 2023

 * PROCESSOR and CONNECTOR healthState: 
     - MR
     - unit tests
     - incorporated CONNECTOR (P4 switch) health state into `Subarray`
     - [PERENTIE-1975](https://jira.skatelescope.org/browse/PERENTIE-1975)
 * PROCESSOR and CONNECTOR healthState: show on Taranta dashboard
     - [PERENTIE-1977](https://jira.skatelescope.org/browse/PERENTIE-1977)
 * expose Alveo temperature/power/etc as Tango attribute
     - [PERENTIE-1978](https://jira.skatelescope.org/browse/PERENTIE-1978)
	
# Week 30 June - 6 July 2023
 * PROCESSOR and CONNECTOR healthState: implementation, debugging
 
    healthState reporting design review
	
    degugging FPGA firmware register update gone wrong, testing new FW build
	
   [PERENTIE-1974](https://jira.skatelescope.org/browse/PERENTIE-1974)
   [PERENTIE-1975](https://jira.skatelescope.org/browse/PERENTIE-1975)
   
# Week 22 - 29 June 2023

 * PROCESSOR and CONNECTOR healthState: analysis, design, prototyping
   [PERENTIE-1974](https://jira.skatelescope.org/browse/PERENTIE-1974)
   [PERENTIE-1975](https://jira.skatelescope.org/browse/PERENTIE-1975)

# Comments
 * see this on [GitLab page](https://gitlab.com/bernardo.bacic/tmp_status/-/tree/main/2023)
